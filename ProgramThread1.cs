using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Thread1
{
    
    class ProgramThread1
    {
       
        static void PrintArray(int []arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine($"{i} {arr[i]}");
            }
        }
        static void EnterMassiv(int CountThread=1)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            int[] array = new int[1000000];
            Task[] taskArray = new Task[CountThread];
            for (int i = 0; i < CountThread; i++)
            {
                int startIterator = array.Length / CountThread * i;
                int endIterator = array.Length / CountThread * (i + 1) ;
               taskArray[i] = Task.Factory.StartNew(()=>
                {
                    for (int j = startIterator ; j <endIterator ; j++)
                    {
                        array[j] = new Random().Next()%10; 
                    }
                   
                }
                );
            }
            Task.WaitAll(taskArray);
            stopWatch.Stop();
            Console.WriteLine($"{stopWatch.ElapsedMilliseconds} миллисекунд потребовалось для заполнения массива в {CountThread} потоках");
        static void Main()
        {
           
           EnterMassiv(1);
           EnterMassiv(2);
           EnterMassiv(4);
        }

    }
}
